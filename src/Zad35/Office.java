package Zad35;

public class Office {
    private int index;

    public Office() {
    }

    public void handle(Client kl) {
        System.out.println("Klient pesel: "+kl.getPesel()+" załatwia sprawę "+kl.getTypSprawy());
    }


    @Override
    public String toString() {
        return "Office{"+ index +
                '}';
    }
}
