package Zad35;

import java.util.Scanner;

public class MainZad35 {
    public static void main(String[] args) {
        Department dep = new Department();
        Scanner sc = new Scanner(System.in);
        String lin = null;

        while (sc.hasNextLine()){
            lin=sc.nextLine();
            parse(lin, dep);
        }


    }
    private static void parse(String lin, Department dep) {
        String[] words = lin.split(" ");

        switch (words[0]) {
            case "add":
                dep.addOffice(new Office());
                break;
            case "obsluz":

                try {
                    String numberOffice = words[1];
                    String pesel = words[2];
                    String hisCase = words[3];
                    Client clientToHandle = new Client(Case.valueOf(hisCase),pesel);
                    int idPokoju = Integer.parseInt(words[1]);
                    dep.getOffice(idPokoju).handle(clientToHandle);
                    int numberOfficeInt = Integer.parseInt(numberOffice);
                    System.out.println(dep.getOffice(numberOfficeInt));
                } catch (NumberFormatException nfe) {
                    System.out.println("Number format exception");
                }

                break;
            case "quit":
                return;
            default:
                System.out.println("Wrong command");
        }
    }
}
