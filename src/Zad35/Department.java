package Zad35;

import java.util.ArrayList;
import java.util.List;

public class Department {
    private List<Office> off = new ArrayList<>();



    public void addOffice(Office office){
      off.add(office);
    }
    public Office getOffice(int index){
        return off.get(index);
    }

    @Override
    public String toString() {
        return "Department{" +
                "dep=" + off +
                '}';
    }
}
