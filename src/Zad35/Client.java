package Zad35;

public class Client {
    private Case typSprawy;
    private String pesel;

    public Client(Case typSprawy, String pesel) {
        super();
        this.typSprawy = typSprawy;
        this.pesel = pesel;
    }

    public Case getTypSprawy() {
        return typSprawy;
    }

    public void setTypSprawy(Case typSprawy) {
        this.typSprawy = typSprawy;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    @Override
    public String toString() {
        return "Client{" +
                "typSprawy=" + typSprawy +
                ", pesel='" + pesel + '\'' +
                '}';
    }
}
